

Pod::Spec.new do |s|
  s.name             = 'GL_Course'
  s.version          = '0.1.0'
  s.summary          = '课程表'


  s.homepage         = 'https://gitee.com/GalenZHL/GL_Course.git'
 
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { '18668088548' => '18668088548@163.com' }
  s.source           = { :git => 'https://gitee.com/GalenZHL/GL_Course.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = 'GL_Course/Classes/**/*'
  
  # s.resource_bundles = {
  #   'GL_Course' => ['GL_Course/Assets/*.png']
  # }

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
