//
//  main.m
//  GL_Course
//
//  Created by 18969420105 on 06/13/2018.
//  Copyright (c) 2018 18969420105. All rights reserved.
//

@import UIKit;
#import "GLAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([GLAppDelegate class]));
    }
}
