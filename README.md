# GL_Course

[![CI Status](https://img.shields.io/travis/18969420105/GL_Course.svg?style=flat)](https://travis-ci.org/18969420105/GL_Course)
[![Version](https://img.shields.io/cocoapods/v/GL_Course.svg?style=flat)](https://cocoapods.org/pods/GL_Course)
[![License](https://img.shields.io/cocoapods/l/GL_Course.svg?style=flat)](https://cocoapods.org/pods/GL_Course)
[![Platform](https://img.shields.io/cocoapods/p/GL_Course.svg?style=flat)](https://cocoapods.org/pods/GL_Course)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

GL_Course is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'GL_Course'
```

## Author

18969420105, zhl@example.com

## License

GL_Course is available under the MIT license. See the LICENSE file for more info.
